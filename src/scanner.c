#include "tree_sitter/parser.h"
#include "tree_sitter/alloc.h"
#include "tree_sitter/array.h"

enum TokenType
{
  BLOCK_START,
  BLOCK_END,
};

typedef struct
{
  /* level indicates how many open blocks do we have at the current lexer position */
  uint8_t level;
  /* blocks_to_close indicates how many BLOCK_END markers should be emitted before lexer can move to the next postion */
  uint8_t blocks_to_close;
} Scanner;

static uint8_t consume_chars(TSLexer *lexer, char c)
{
  uint8_t count = 0;
  while (lexer->lookahead == c)
  {
    lexer->advance(lexer, false);
    ++count;
  }
  return count;
}

static void emit_block_end(TSLexer *lexer, Scanner *s)
{
  s->blocks_to_close--;
  s->level--;
  lexer->result_symbol = BLOCK_END;
}

void *tree_sitter_logseq_external_scanner_create(void)
{
  Scanner *scanner = ts_malloc(sizeof(Scanner));
  scanner->level = 0;
  scanner->blocks_to_close = 0;
  return scanner;
}

void tree_sitter_logseq_external_scanner_destroy(void *payload)
{
  Scanner *scanner = (Scanner *)payload;
  ts_free(scanner);
}

unsigned tree_sitter_logseq_external_scanner_serialize(
    void *payload,
    char *buffer)
{
  Scanner *scanner = (Scanner *)payload;
  buffer[0] = scanner->level;
  buffer[1] = scanner->blocks_to_close;
  return 2;
}

void tree_sitter_logseq_external_scanner_deserialize(
    void *payload,
    const char *buffer,
    unsigned length)
{
  Scanner *scanner = (Scanner *)payload;
  if (length > 0)
  {
    scanner->level = buffer[0];
    scanner->blocks_to_close = buffer[1];
  }
}

bool tree_sitter_logseq_external_scanner_scan(
    void *payload,
    TSLexer *lexer,
    const bool *valid_symbols)
{
  Scanner *s = (Scanner *)payload;

  if (valid_symbols[BLOCK_END])
  {
    if (s->blocks_to_close > 0)
    {
      emit_block_end(lexer, s);
      return true;
    }

    // we might close blocks after new line
    if (lexer->lookahead == '\n')
    {
      lexer->advance(lexer, false);
      uint8_t tabs = consume_chars(lexer, '\t');
      if (tabs < s->level)
      {
        s->blocks_to_close = s->level - tabs;
        emit_block_end(lexer, s);
        return true;
      }
    }
    // we will close all open blocks at the end of the file
    else if (lexer->eof(lexer))
    {
      s->blocks_to_close = s->level;
      emit_block_end(lexer, s);
      return true;
    }
  }

  if (valid_symbols[BLOCK_START])
  {
    if (lexer->lookahead == '-')
    {
      lexer->advance(lexer, false);
      if (lexer->lookahead == ' ')
      {
        // opening a block increases the global scanner level
        s->level++;
        lexer->advance(lexer, false);
        lexer->result_symbol = BLOCK_START;
        return true;
      }
      return false;
    }
  }
  return false;
}
