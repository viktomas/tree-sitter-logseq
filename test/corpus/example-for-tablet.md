=====
Simplified
=====

- hello
- world

---

(document
  (block (block_start) (block_content) )
  (block (block_start) (block_content) ))

=====
Nested single-line blocks
=====

- hello
	- second
		- third
	- another
- world

---

(document
  (block (block_start) (block_content)
    (block (block_start) (block_content)
      (block (block_start) (block_content)))
    (block (block_start) (block_content)))
  (block (block_start) (block_content)))
