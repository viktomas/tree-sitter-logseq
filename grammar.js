module.exports = grammar({
  name: "logseq",

  // Skip carriage returns.
  // we only care about \n
  extras: (_) => ["\r"],

  externals: ($) => [$.block_start, $._block_end],

  rules: {
    document: ($) => seq(/\s*/, repeat($.block)),

    block: ($) =>
      seq($.block_start, $.block_content, repeat($.block), $._block_end),

    block_content: ($) => /[^\n]+/,
  },
});
