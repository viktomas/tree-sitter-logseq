Prompt for AI:

We will write a tree-sitter grammar that will parse these example files.

I'm showing you all examples, but I want to start simple, with a grammar that will parse this case:

```
- one block```

A few notes for you

- The document is made out of blocks (starting with -)
- You can also have nested blocks (indented -)
- you can have multiline blocks
- as you can see, the last line of the file is not terminated with newline character, that's why the last character of the line is followed directly by ```


```
-
- one block
-```

```
-```



```
- multi
  line
  block```

```
- one block
        - one sub block```

```
- one block```

```
-
-
-```


## Backup test

=====
First example
=====

- hello
	- second
		- third
- world

---

(document
  (block_start text
    (block_start text
      (block_start text)))
  (block_start text))
