=====
Block in the middle
=====

-
- one block
-

---

(source_file
  (block)
  (block
    (block_content))
  (block))
